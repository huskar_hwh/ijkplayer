#!/bin/bash
echo "进入编译ffmpeg脚本"
function buildFF
{
	echo "开始编译ffmpeg"
	./configure \
    --prefix=$PREFIX \
    --target-os=android \
    --cross-prefix=$CROSS_COMPILE \
    --arch=$ARCH \
    --sysroot=$SYSROOT \
    --extra-cflags="$CFLAG" \
    --cc=$CC \
    --nm=$NM \
    --enable-shared \
    --enable-runtime-cpudetect \
    --enable-gpl \
    --enable-small \
    --enable-cross-compile \
    --disable-debug \
    --disable-static \
    --disable-doc \
    --disable-ffmpeg \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-postproc \
    --disable-avdevice \
    --disable-symver \
    --disable-stripping \
    $ADD
	make clean
    make -j16
    make install
	echo "编译结束！"
}
###########################################################


######共有属性
ANDROID_VERSION=24
NDK=/home/hwh/android-ndk-r14b
ADD="--enable-asm \
    --enable-neon \
    --enable-jni \
    --enable-mediacodec \
    --enable-decoder=h264_mediacodec \
    --enable-hwaccel=h264_mediacodec "
######
if [ $1 = 'arm' ]
then
	echo "编译支持neon和硬解码 armv7-a"
	#armv7-a
	ARCH=arm
	CPU=armv7-a
	TOOLCHAIN=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64
	CC=$TOOLCHAIN/bin/arm-linux-androideabi-gcc
	NM=$TOOLCHAIN/bin/arm-linux-androideabi-nm
	PREFIX=./android/$CPU-neon-hard
	SYSROOT=$NDK/platforms/android-$ANDROID_VERSION/arch-$ARCH/
	CROSS_COMPILE=$TOOLCHAIN/bin/arm-linux-androideabi-
	CFLAG="-Os -fPIC -DANDROID -march=$CPU -mfpu=neon -mfloat-abi=softfp "
	buildFF
fi

if [ $1 = 'arm64' ]
then
	echo "编译支持neon和硬解码 arm64-v8a"
	#arm64-v8a
	ARCH=arm64
	CPU=armv8-a
	TOOLCHAIN=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64
	CC=$TOOLCHAIN/bin/aarch64-linux-android-gcc
	NM=$TOOLCHAIN/bin/aarch64-linux-android-nm
	PREFIX=./android/$CPU-hard
	SYSROOT=$NDK/platforms/android-$ANDROID_VERSION/arch-$ARCH/
	CROSS_COMPILE=$TOOLCHAIN/bin/aarch64-linux-android-
	CFLAG="-Os -fPIC -DANDROID -march=$CPU "
	buildFF
fi


if [ $1 = 'x86' ]
then
	echo "编译支持x86"
	#x86
	ARCH=x86
	CPU=i686
	TOOLCHAIN=$NDK/toolchains/x86-4.9/prebuilt/linux-x86_64
	CC=$TOOLCHAIN/bin/i686-linux-android-gcc
	NM=$TOOLCHAIN/bin/i686-linux-android-nm
	PREFIX=./android/$CPU
	SYSROOT=$NDK/platforms/android-$ANDROID_VERSION/arch-$ARCH/
	CROSS_COMPILE=$TOOLCHAIN/bin/i686-linux-android-
	CFLAG="-Os -fpic -DANDROID -march=$CPU -mtune=intel -mssse3 -mfpmath=sse -m32 "
	buildFF
fi

if [ $1 = 'x86_64' ]
then
	echo "编译支持x86_64"
	#x86_64
	ARCH=x86_64
	CPU=x86-64
	TOOLCHAIN=$NDK/toolchains/x86_64-4.9/prebuilt/linux-x86_64
	CC=$TOOLCHAIN/bin/x86_64-linux-android-gcc
	NM=$TOOLCHAIN/bin/x86_64-linux-android-nm
	PREFIX=./android/$CPU
	SYSROOT=$NDK/platforms/android-$ANDROID_VERSION/arch-$ARCH/
	CROSS_COMPILE=$TOOLCHAIN/bin/x86_64-linux-android-
	CFLAG="-Os -fpic -DANDROID -march=$CPU -msse4.2 -mpopcnt -m64 -mtune=intel "
	buildFF
fi
